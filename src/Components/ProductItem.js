import React, { Component } from 'react'

export default class ProductItem extends Component {
  render() {
    return (
      <div>
          <img src={this.props.image} alt={this.props.name}/>
           {this.props.name}<br/>
           {this.props.description}
      </div>
    )
  }
}
