import React from 'react'
import {Link} from 'react-router-dom'

export default function Header() {
  return (
    <div>
        <nav>
            <Link to="/"> Home </Link>
            <Link to="products"> Products </Link>
            <Link to="data"> Data </Link>
            <Link to="about"> About </Link>
        </nav>
       <h1>Welcome to Music shop</h1> 
       <hr/>

    </div>   
  )
}
