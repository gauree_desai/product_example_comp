import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductItem from './ProductItem';
import fetch_products from '../actions/ProductActions' 

 class ProductList extends Component {

  // constructor(){
  //   super();
  //   this.state={
  //     products:data.products
  //   }
  // }


 componentDidMount(){
   console.log("Product list is mounted... so called a prop which called dispatch action")
   this.props.fetchProducts()
 }

  render() {
    const list= this.props.products ?.map((item)=> <ProductItem key={item.name} image={item.img} name={item.name} description={item.description}></ProductItem>)
    return (
      <div>{list}</div>
    )
  }
}

const mapStateToProps=(state)=>{
  console.log(state)
  return {
     products:state.preducer
  }
}


const mapDispatchToProps=(dispatch)=>{
  return {
     fetchProducts:()=>dispatch(fetch_products())
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(ProductList) 

