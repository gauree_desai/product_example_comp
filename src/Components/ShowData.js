import React, { Component } from 'react'
import { connect } from 'react-redux';
import fetch_data from '../actions/DataAction'

class ShowData extends Component {
  render() {
    const data= this.props.data?.map((item)=>item.name+" ") 
    return (
       <div>
             {data}
             <button onClick={()=>this.props.showData()}>Click me to see Data</button>
       </div>
    

    )
  }
}

const mapStateToProps = (state) => {
    console.log("map state to prop",state)
    return {
        data:state.dreducer
    }
}

const mapDispatchToProps =(dispatch)=> {
    return {
      showData:()=>dispatch(fetch_data())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowData)