import { combineReducers } from "redux";
import preducer from './ProductReducer';
import dreducer from './DataReducer';

const rootReducer= combineReducers({
    preducer,
    dreducer
})


export default rootReducer;