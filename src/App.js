import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import ProductList from './components/ProductList';
import Footer from './components/Footer';
import {Routes, Route,Switch} from 'react-router-dom'

import Home from './components/Home';
import AboutUs from './components/AboutUs';
import PageNotFound from './components/PageNotFound';
import ShowData from './components/ShowData';

function App() {
  return (
    <div className="App">
     <Header></Header>
     <Routes>
       
          <Route path="/" element={<Home />} />
          <Route path="products" element={<ProductList />} />
          <Route path="about" element={<AboutUs />} />
          <Route path="data" element={<ShowData/>}/>
          <Route path="*" element={<PageNotFound/>}/>
     
    </Routes>
     
     <Footer></Footer>
    </div>
  );
}

export default App;
