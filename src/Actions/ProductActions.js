import json from '../data/MusicProducts.json'

export default function fetch_products(){
    console.log("fetch_Products action called.. generated action object",json.products)
    return{
        type:'FETCH_PRODUCTS',
        payload:json.products
    }
}