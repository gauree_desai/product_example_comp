import { configureStore } from '@reduxjs/toolkit'
import reducers from '../reducers'

let store= configureStore({reducer:reducers})


export default store;